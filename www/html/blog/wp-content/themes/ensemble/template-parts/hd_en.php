<div class="navbar navbar-expand-lg   navbar-ens  p-0 px-md-4 sticky-top">
    <div class="container ">
      <div class="d-flex align-items-center" ><a href="/"><img src="/assets/images/EnsembleApp_logo.svg" alt="" width="113"/></a></div>
		<button  class="mobNav " type="button"><span></span><span class="dsp"></span><span></span><span class="dsp"></span><span></span><span class="dsp"></span><span></span><span class="dsp"></span><span></span></button>
		
		<div class="menuOptions">
   <ul class="navbar-nav ">
      <li class="nav-item hg ">
        <a class="nav-link" href="/create_event.html" id="KeyBenefits" >Key Benefits</a>
		  <div class="dropdown-menu secNv" aria-labelledby="KeyBenefits">
			  <div class="container px-5">
		   <ul >
			  <li><a href="/create_event.html" >Create</a></li>
			   <li><a href="/manage_event.html">Manage</a></li>
			   <li><a href="/engage.html">Engage</a></li>
			   <li><a href="/discover.html">Discover</a></li>
			   
			  </ul>
		  </div>
		  </div>
      </li>
      <li class="nav-item hg">
        <a class="nav-link " href="/price.html">Pricing</a>
      </li>
    
    <li class="nav-item hg active">
        <a class="nav-link" href="/blog/">Expert Insights</a>
      </li>
		 <li class="nav-item hg">
        <a class="nav-link" href="/aboutus.html">About Ensemble</a>
		</li>
		<div class="d-flex ctas order-lg-0 order-md-1 order-sm-1 order-1">
		 <li class="nav-item">
        <a class="btn button-orange button-translate" href="/demo.html">Check our demo</a>
      </li></div></ul>
  </div>

    
		
		
		 
    </div>
  </div>
	
	